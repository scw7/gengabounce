# The GENGA Code: Gravitational Encounters with GPU Acceleration.
** Authors: Simon Grimm and Joachim Stadel **


** Institute for Computational Science **
** University of Zurich **
** Switzerland **

[TOC]

# License #
GENGA is free to use, but when results from GENGA are published, then the following paper has to be referenced: 
The GENGA Code: Gravitational Encounters in N-body simulations with GPU Acceleration (Grimm & Stadel 2014).

# Requirements #

GENGA runs on Nvidia GPUs with compute capability of 2.0 or higher. To be able to use the Code one has to install the Cuda Toolkit. This can be downloaded from https://developer.nvidia.com/cuda-downloads.
We strongly recommend to use the driver version 5.0 or higher to get the full performance and correct results. Especially in version 3 and 4 have been observed rounding errors which can affect the output of a simulation serious. 


# Compilation #
GENGA must be compiled for a specific GPU compute capability. The compute capability corresponds to the GPU generation, and a list of all Nvidia GPUS with their compute capabilities can be found here: https://developer.nvidia.com/cuda-gpus .

All files needed for compilation are located in the source directory. GENGA can then be compiled in the source directory with the given Makefile by typing 'make SM=xx' to the terminal, where xx corresponds to the compute capability. For example use 'make SM=20' for compute capability of 2.0, or 'make SM=35' for 3.5. 

For example use:
make SM=35 for Tesla K20 
make SM=52 for GeForce GTX 980
make SM=60 for Tesla P100
make SM=61 for GeForce GTX 1080 ti

When compiling GENGA with the openGL real time visualization, go to the GengaGL directory. 
When GENGA is compiled for a newer compute capability then the GPU is able to run, the the following error message will appear by running GENGA:
`FGAlloc  error = 13 = invalid device symbol`.
When GENGA is used with an old CUDA version (<9), then in the 'define.h' file the option 'OldShuffle' needs to be set to 1.

## GCC version ##
It can happen that the used CUDA version needs an alder GCC version than the current one on the system. In that case, either a newer CUDA version, or an older gcc version should be installed. Use the following compile option to tell CUDA to use an older GCC version (in that example 7.0):
`-ccbin=g++-7`.


# Starting GENGA #
GENGA can be startet with

```
./genga
```
followed by optional arguments listed [here](#markdown-header-console-arguments).
When GENGA is started, it creates a Lock file named lock.dat. This file must be deleted when GENGA is started again without a restart time greater than 0. This prevents from data loss by an accidental relaunch. To ignore the lock file the IgnoreLockFile Flag in the define.h file can be set to 1.


# The param.dat File #
Simulation parameters are specified in the 'param.dat' file. The used parameters are listed here, the order can also be changed. If a line in the 'param.dat' file is missing, then the corresponding default value is taken from the 'define.dat' file.

 * The time step, in days
 * The output name
 * The Energy output interval, in time steps. When it is set to zero, then no energy outputs are written. When set to -1, then only the last energy output  is written.
 * The Coordinates output interval, in time steps. When it is set to 0, then no coordinate outputs are written. When set to -1, then only the last time step is written.
 * The number of outputs per Coordinate output interval.
 * The Coordinates output buffer, in time steps. If this is larger than 1, then the coordinate outputs are written only block-wise to increase the performance especially in the multi simulation mode with lots of consecutive outputs. The energy outputs within a buffer size are skipped in this mode.  
 * The file name of irregular coordinate output calendar. "-" means no irregular outputs. See [here](#markdown-header-irregular-coordinate-output) for more details. 
 * The total number of integration steps
 * The central Mass, in solar masses
 * Value of close encounter parameter n1
 * Value of close encounter parameter n2
 * The Star Radius in AU
 * The Star Love Number
 * The Star fluid Love Number
 * The Star spin_x in Solar masses AU^2 / day * 0.0172020989
 * The Star spin_y in Solar masses AU^2 / day * 0.0172020989
 * The Star spin_z in Solar masses AU^2 / day * 0.0172020989
 * The Star tau (time lag) in day / 0.0172020989.
 * The Solar Constant at 1 AU in W /m^2. 
 * The Input file name
 * The input file format:

    The possible arguments are listed below, some are optional, the order can be changed. The input file format arguments in the param.dat file must be placed between '<< ' and ' >>' and separated with a space.  
    Cartesian corrdinates can not be mixed with Keplerian elements.

    * x = x-coordinate in AU (heliocentric)
    * y = y-coordinate in AU (heliocentric)
    * z = z-coordinate in AU (heliocentric)
    * m = mass in Solar masses
    * vx = x-velocity in AU/day * 0.0172020989 (heliocentric)
    * vy = y-velocity in AU/day * 0.0172020989 (heliocentric)
    * vz = z-velocity in AU/day * 0.0172020989 (heliocentric)
    * a = semi-major axis in AU
    * e = eccentricity
    * inc = inclination in radians
    * O = (Omega) longitude of the ascending node in radians
    * w = (omega) argument of periapsis in radians
    * M = mean anomaly in radians
    * rho = density in g/cm^3; optional, the default value can be specified below.
    * r = physical radius in AU; optional, if r is not given or the radius is equal to zero, then the program uses the density to calculate the radius. Note that you have many ways to input the radius or density. Look at the input file section.
    * Sx = x-spin in Solar masses AU^2 / day * 0.0172020989; optional, the default value is 0.0
    * Sy = y-spin in Solar masses AU^2 / day * 0.0172020989; optional, the default value is 0.0
    * Sz = z-spin in Solar masses AU^2 / day * 0.0172020989; optional, the default value is 0.0
    * i = index of the body; optional, the default value is the line number in the input file. Don't give two bodies the same index.
    * t = start time of the simulation in years, optional. The default is zero.
    * amin = minimal value of semi major axis range for aecount; optional, the default value is 0.0. See [here](#markdown-header-aelimits) for more details
    * amax = maximal value of semi major axis range for aecount; optional, the default value is 100. See [here](#markdown-header-aelimits) for more details
    * emin = minimal value of eccentricity range for aecount; optional, the default value is 0.0. See [here](#markdown-header-aelimits) for more details
    * emax = maximal value of eccentricity range for aecount; optional, the default value is 1.0. See [here](#markdown-header-aelimits) for more details
    * k2 = potential Love number of degree 2
    * k2f = fluid Love number of degree 2
    * tau = time lag in day / 0.0172020989.
    * \- = skip this column, optional

 * The angle units: "radians" or "degrees".
 * The default value of the density in g/cm^3. If the density is not included in the initial condition file, it can be set here globally for all bodies.
 * Use the Test particle mode: 0: All Bodies are treated as massive bodies; 1: Small bodies does not affect big bodies; 2: Small bodies only affect large bodies.
 * Particle minimum Mass:  Minimal Mass for massive particles in test particle mode, lighter or equal sized particles are treated as test particles.
 * The Restart time step. If it's set to a value bigger than 0, then the simulation will continue at the specified time step.
 * The minimal number of bodies in the simulation, not counting test particles. If the number of bodies gets smaller than Nmin, then the simulation will stop.
 * The minimal number of test particles in the simulation. If the number of test particles gets smaller than NminTP, then the simulation will stop.
 * The inner truncation radius in AU, bodies with a separation to the Sun smaller than this are taken out of the simulation. (RcutSun in older versions)
 * The inner truncation radius in AU, bodies with a separation to the Sun smaller than this are taken out of the simulation. (RcutSun in older versions)
 * The outer truncation radius in AU, bodies with a separation to the Sun larger than this are taken out of the simulation. (Rcut in older versions)
 * The Order of the symplectic integrator. The options are 2, 4 or 6
 * The maximum number of close encounter pairs for each body
 * Use aeGrid, 0 or 1. See [here](#markdown-header-aegrid) for more details
 * Mininal major axis for the aeCount grid. See [here](#markdown-header-aegrid) for more details
 * Maximal major axis for the aeCount grid. See [here](#markdown-header-aegrid) for more details
 * Mininal eccentricity for the aeCount grid. See [here](#markdown-header-aegrid) for more details
 * Maximal eccentricity for the aeCount grid. See [here](#markdown-header-aegrid) for more details
 * Mininal inclination for the aeCount grid. See [here](#markdown-header-aegrid) for more details
 * Maximal inclination for the aeCount grid. See [here](#markdown-header-aegrid) for more details
 * The number of cells in a of the aeCount grid. See [here](#markdown-header-aegrid) for more details
 * The number of cells in e of the aeCount grid. See [here](#markdown-header-aegrid) for more details
 * The number of cells in i of the aeCount grid. See [here](#markdown-header-aegrid) for more details
 * The time step when aeCount starts. (In Detail aeCount will start at the next bigger coordinate output step).
 * The name for the aeCount grid
 * Use gas disk: By setting this to 1, the [gas disc](#markdown-header-gas-disc) is used. Individual effects can be selected with the following parameters.
   When set to 0, then no gas effects are applied. 
 * Use gas disk potential; By setting this to 1, the gas disk potential is applied to all particles.
   When set to 2, then the effect is limited to particles with m < Mgiant.
 * Use gas disk enhancement; By setting this to 1, the gas disk enhancement is applied to all particles with m < def_M_Enhance and m > def_MgasSmall.
   When set to 2, then the effect is limited to particles with m < Mgiant.
 * Use gas disk drag; By setting this to 1, the gas disk drag applied to all particles.
   When set to 2, then the effect is limited to particles with m < Mgiant.
 * Use gas disk tidal dampening; By setting this to 1, the gas disk tidal damping (Type I migration) is applied to all particles.
   When set to 2, then the effect is limited to particles with m < Mgiant.
 * Gas dTau_diss: The dissipation time for the gas disc in years
 * Gas Sigma_10: The gas surface density at 1 AU Sigma_10 in g / cm^3
 * Gas alpha: The power law exponent for the gas disc, alpha 
 * Gas Mgiant: Mass limit for gas effects, in solar masses. If m > Mgiant, the gas drag and tidal dampening is not applied to that particle.
 * Gas file name: name of file containing time, r, Sigma and h.
 * Use force: use additional force, which is specified in the file force.h. See [here](#markdown-header-additional-forces) for more details.
 * Use Yarkovsky. 0: No Yarkovsky effect, 1: Yarkovsky effect (dv/dt), 2: Yarkovsky effect (da/dt) See [here](#markdown-header-yarkovsky-effect) for more details.
 * Use Poynting-Robertson. 0: No PR drag, 1: PR drag (dv/dt), 2: PR drag (da/dt, de/dt) See [here](#markdown-header-poynting-robertson-drag) for more details.
 * Radiation Pressure Coefficient Qpr: Efficiency factor, used for Poynting Robertson drag.
 * Use Small Collisions. 0: no effect, 1: Fragmentation and rotation rate reset model for test particles.
 * Set Elements file name: '-': no file, 'name':  name of the file containg the data table for Keplerian elements. See [here](#markdown-header-set-elements-function) for more details. 
 * FormatS: Output file format for multi simulation run. 0: all simulations write to different files, 1: all simulations write to the same file.
 * FormatT: Output file format for time steps. 0: all time steps are written to different files, 1: all time steps are written to the same file.
 * FormatP: Output file format for particles. 0: all particles are written to different files, 1: all particles are written to the same file.
 * FormatO: Output file format for file names. 0: file names contain time steps, 1: file names contain output steps.
 * Report Encounters: When this is set to one, then close encounters with a separation less than a factor f times the physical radius of the body are reported to a file.
 * Report Encounters Radius: This option sets the factor f of the 'Report Encounter'  separation.
 * Stop at Encounter: When this is set to one, then the simulation or sub-simulation is stopped when the separation between two bodies is less than a factor g times the Hill radii of the involved bodies. 
 * Stop at Encounter Radius. This option sets the factor g of the 'Stop at Encounter' sepparation.  
 * Stop at Collision. 1: stop simulation when a collision occurs; 0: continue simulation with merged bodies (default)
 * Stop Minimum Mass. when `Stop at Collision` = 1, then stop simulations only when both bodies are more massive than `Stop Minimum Mass`.
 * Collision Precision. Tolerance for Collision Time Precision, In days. The default values is 1.0
 * Collision Time Shift. Print the collision coordinates before the collision happens, when the involved bodies are sepparated by `Collision time Shift` times the physical radii. The default value is 1.0.
 * Serial Grouping. By setting this flag to 1, simulations can be exactly reproduced, but the performance can be slower.


# Console Arguments #
Instead of using the parameter file, some arguments can also be passed as console arguments. The console arguments have the highest priority and are overwriting the arguments of the param.dat file. The options are:

 * \-dt f Time step in days
 * \-ei i Energy output interval
 * \-ci i Coordinates output interval
 * \-I i Number of integration steps
 * \-n1 f Value of n1
 * \-n2 f Value of n2
 * \-dev i Device number
 * \-in s Input file name
 * \-out s Output name
 * \-R i Restart a simulation at time step i
 * \-TP i Test Particle mode i = 0, treat all bodies the same way, i = 1: small bodies don't affect big bodies.
 * \-M s name of file, containing a list of the directories for the multi simulation mode.
 * \-Nmin i Minimal number of bodies in the simulation, not including test particles.
 * \-NminTP i Minimal number of test particles in the simulation.
 * \-SIO i Order of symplectic integrator, The options are 2, 4 or 6.
 * \-aeN s Name of the aeCount grid
 * \-t f Start time of the simulation in years 

Here i means an integer, f a floating point value, and s a string. 

# Defined Constants and Flags in the define.h file #

Some Constants are defined as c++ preprocessor directives in the define.h file. After changing one of these constants, the code has to be recompiled.

 * All default parameters for the 'param.dat' file. See [here](#markdown-header-the-param.dat-file) for more details

 * def_pc f: Factor in Pre-checker, Pairs with rij^2 smaller than pc * rcrit^2 are considered as close encounter candidates
 * def_MaxColl i: Maximum number of Collisions per time step that can be stored
 * def_cef f: Close encounter factor, pairs with rij^2 smaller than f * rcrit^2 are considered as close encounter pairs.
 * def_tol f: Tolerance in Bulirsh Stoer integrator.
 * def_dtmin f: Minimal time step in Bulirsch Stoer integrator .
 * def_Nfragments d: Additional array size for debris particles.
 * def_NFileNameDigits: Number of time step digits in the output filenames.
 * poincareFlag i: By setting this flag, the [Poincare surface of section](#markdown-header-the-poincare-surface-of-section) is used.
 * IgnoreLockFile i: By setting this flag, the lock file is ignored and simulation can always be started again.
 * def_GMax i: Defines the maximum size of close encounter groups as 2^GMax.
 * Asteroid_eps f: Emissivity factor
 * Asteroid_rho f: Density of body in kg/m^3, This is only used when the mass is 0. When the mass is greater than 0, then the density is taken from the initial conditions file.
 * Asteroid_C f: Specific Heat Capacity in J/kgK
 * Asteroid_A f: Bond albedo
 * Asteroid_K f: Thermal conductivity in W/mK



Here i means an integer and f a floating point value. 

# The Initial Conditions #
The initial conditions are read from the file specified in 'param.dat' (Input file = ). The initial conditions must be a text file and the format must correspond to the values set in the 'param.dat' file (Input file Format: << ... >> ). The data of each particle has to be written in a new lines in text format. Don't write the central mass (Sun) in the initial condition file, it is set automatically to the heliocentric origin with a mass specified in the 'parm.dat' file. 

 * Example 1
    
    * Format in 'param.dat': << x y z m vx vy vz r >>
    * input file:
        * x1 y1 z1 m1 vx1 vy1 vz1 r1
        * x2 y2 z2 m2 vx2 vy2 vz2 r2
        * .
        * .
        * .
        * xn yn zn mn vxn vyn vzn rn

    GENGA reads the radii from the initial condition file. 

 * Example 2

    * Format in 'param.dat': << x y z m vx vy vz rho >>
    * input file:
        * x1 y1 z1 m1 vx1 vy1 vz1 rho1
        * x2 y2 z2 m2 vx2 vy2 vz2 rho2
        * .
        * .
        * .
        * xn yn zn mn vxn vyn vzn rhon

    GENGA reads the densities from the initial condition file and computes the radii.

 * Example 3

    * Format in 'param.dat': << x y z m vx vy vz >>
    * Default rho = 2.0
    * input file:
        * x1 y1 z1 m1 vx1 vy1 vz1
        * x2 y2 z2 m2 vx2 vy2 vz2
        * .
        * .
        * .
        * xn yn zn mn vxn vyn vzn

    GENGA uses the default density from the 'param.dat' file and computes the radii.

 * Example 4

    * Format in 'param.dat': << x y z m vx vy vz r rho >>
    * Default rho = 2.0
    * input file:
        * x1 y1 z1 m1 vx1 vy1 vz1 r1 rho1
        * x2 y2 z2 m2 vx2 vy2 vz2 r2 rho2
        * .
        *  .
        * .
        * xn yn zn mn vxn vyn vzn r2 rho2

    GENGA reads the radii from the initial condition file. If a radius set to zero, then GENGA reads density from the initial condition file and computes the radius. 

# The Output Files #
## The Coordinate output files
This file contains the heliocentric positions and velocities, the spin and some information about the orbit and close encounters.
At the coordinate output interval, set in the 'param.dat' file, a new output is written. The structure of the coordinate output files depends on the parameters FormatS, FormatT ,FormatP and FormatO. Here we describe the possible choices: 

### FormatS = 0, FormatT = 0, FormatP = 1, FormatO = 0: Out<name>_<time step>.dat
In this format, at each coordinate output interval, a new file is created which contains all particles. In the multi simulation mode, each sub simulation folder contain individual files.

    t i1 m1 r1 x1 y1 z1 vx1 vy1 vz1 Sx1 Sy2 Sz1 amin1 amax1 emin1 emax1 aecount1 aecountT1 enccountT1 test1
    t i2 m2 r2 x2 y2 z2 vx2 vy2 vz2 Sx2 Sy2 Sz2 amin2 amax2 emin2 emax2 aecount2 aecountT2 enccountT2 test
    .
    .
    .
    t in mn rn xn yn zn vxn vyn vzn Sxn Syn Szn aminn amaxn eminn emaxn aecountn aecountTn enccountTn testn

 * t is the time in years.
 * i is the particle index. If two particles collide, then the new index is the one from the more massive particle. If both particles have the same mass, then the smaller index is taken.
 * m is the mass of the body in Solar masses.
 * r is the physical radius of the body in AU.
 * x, y, z are the heliocentric positions in AU.
 * vx, vy, vz are the heliocentric velocities in AU/day * 0.0172020989.
 * Sx, Sy, Sz are the spin components in Solar masses AU^2 / day * 0.0172020989.
 * amin, amax, emin and emax are the specified boundaries for the aeCount box, see [here](#markdown-header-aelimits) for more details. At a collision, the same rules as for the index are applied to these values.
 * aecount is the number of time steps since the last coordinate output time in which the particles semi major axis and eccentricity where in the aecount box limits. See [here](#markdown-header-aelimits) for more details.
 * aecountT is the integrated value of all previous aecount values.
 * enccountT is the number of time steps since the simulation start in which the particle was in a close encounter with another (massive) particle.
 * test can be used for some individual values

### FormatS = 1, FormatT = 0 FormatP = 1, FormatO = 0: Out<name>.dat
Here the difference is that in the multi simulation mode, the coordinates are not written in the sub simulation folders, but in the main folder. The output files contain all particles from all sub simulations.
### FormatS = 0, FormatT = 1 FormatP = 1, FormatO = 0: Out<name>.dat
Here all the time steps are written to the same file, containing all time steps and all particles.
### FormatS = 1, FormatT = 1 FormatP = 1, FormatO = 0: Out<name>.dat
Here all time steps are written to the same file, containing all time steps and all particles from all sub simulations.
### FormatP = 0: Outp< index>.dat
Here all particles are written to different files, containing all time steps. 
### FormatS = 0, FormatT = 0, FormatP = 1, FormatO = 1: Out<name>_<output step>.dat
Here the difference is that the output files are not named after the time step, but the output step. When the simulation is interrupted at a time step in between of two output steps, then a backup file 'Outbackup<name>_<time step>.dat' is created. This backupstep step can be read by the restart option -R -1. To restart from a nomrmal putput file, the real time step, and not the output step must be chosen. 
### FormatT = 0, and FormatP = 0
This option is not possible, it is equivalent to FormatT = 1 and FormatP = 0


## The Energy output file: Energy<name>.dat
This file contains information about the number of particles and the energy. At the energy output interval set in the 'param.dat' file, a new line in this file is written. The format is the following:

    time N V T LI U ETotal LTotal LRelativ ERelativ
    .
    .
    .

 with

 * time in years
 * N: Number of particles
 * V: Total potential energy
 * T: Total Kinetic energy
 * LI: Angular momentum lost at ejections
 * U: Inner energy created from collisions, ejections or gas disc
 * ETotal: Total Energy
 * LTotla: Total Angular Momentum
 * LRelativ: (LTotal_t - LTotal_0)/LTotal_0
 * ERelativ: (ETotal_t - ETotal_0)/ETotal_0

## The Execution time file: time<name>.dat
This file contains the execution time spent for the corresponding Coordinate Output interval, in seconds. The last line contains the total execution time in seconds.
The first columns indicates the time step. This last entry in this file is used for the automated restart (restart timestep = -1).

##The information file: info<name>.dat
This file contains general information about the used parameters and hardware.
This file contains also information about the number of close encounter pairs and close encounter group sizes: It shows the Number of prechecked close encounter pairs, the number of close encounter pairs, and the number of groups of sizes 2,4,8,16,32,64,128,256,512,1024 and 2048.
If an error occurs during the simulation, then in this File is written the last coordinates-Output and an information about the error. 

##The collision file: Collisions<name>.dat
In this file are listed the details of the collisions between particle i and j just after they collide.
The precision of the collision output can be adjuted with the `Collision Precision` argument in the 'define.h' file. A value of 1.0 prints the position at the Bulirsh stoer step just after the collision. A Value of 1.0e7 refines the Bulirsch Stoer steps down to 1.0e7 days. This value should not be smaller than def_dtmin.

    time indexi mi ri xi yi zi vxi vyi vzi Sxi Syi Szi indexj mj rj xj yj zj vxj vyj vzj Sxj Syj Szj
    .
    .
    .

##The full collision output file: OutCollison.dat
This file is only written when the `Stop at Collision` argument is set to 1. This file contains the coordinates of all the bodies at the collision time of two individual bodies. The output time can be moved before the actual collision with the `Collision Time Shift` argument. When this argument is larger than 1.0, then the coordinates are reported at the time when the separation between the two bodies was `Collision Time Shift` times the planetary radii.


## The ejection file Ejections<name>.dat
In this file are listed the details of the ejected particles. An ejection happens if the distance of a particle to the central mass is greater than the outer truncation radius (Rcut) value specified in the 'defines.h' file, or smaller than the inner truncation radius (RcutSun) value.

    time index m r x y z vx vy vz Sx Sy Sz case
    .
    .
    .

    with: case = -3 for ejected bodies, and case = -2 for bodies falling into the central mass. 


## The encounter file Encounters<name>.dat
This file is only generated when the 'Report Encounters' argument in the 'param.dat' file is greater than zero. It contains the coordinates at the time step just before the closest encounter between two bodies occurs.

    time indexi mi ri xi yi zi vxi vyi vzi Sxi Syi Szi indexj mj rj xj yj zj vxj vyj vzj Sxj Syj Szj
    .
    .
    .


## The aeGrid file: aeCount<aeGrid name>_<time step>.dat
If the use_aeGrid value in the 'param.h' file is set to one, then at the coordinate output interval, a new aeGrid file is created. This file contains two matrices of the size Na x Ne followed by two matrixes of the size Na x Ni, where Na, Ne and Ni are set in the 'param.dat' file. The first matrix contains the number of time steps which a particle was at the corresponding semi major axis and eccentricity bin, since the last output time
The second matrix contains the same information since the beginning of the simulation.
The next to matrices contain the same for the inclination.

In the multi simulation mode all sub simulations are contributing to the same files.
See [here](#markdown-header-aegrid) for more details.

## The Poincare surface of section file: Poincare<name><timeIntervall>.dat
The file contains the following informations (see [here](#markdown-header-the-poincare-surface-of-section) for more details

    time index x vx
    .
    .
    .

The crossing events are written consecutively the the file. After each coordinate output interval another file is created to reduce the file sizes.

##The Fragments<name>.dat file##
This file is only written with using 'UseSmallCollisions = 1' in the 'param.dat' file (only working for test particles).
It contains information about fragmentation and rotation reset events.

    time index m r x y z vx vy vz Sx Sy Sz event 
    .
    .
    .
    .

the 'event' indicates the following:
 * 0: rotation rate reset 
 * -1: Collision, the particle is destroyed, and it is replaced with fragments (listed in the next lines with event=1 or event=2 of this file)
 * 1: A new fragment particle. The original body is the last body in this file with event = -1.
 * 2: A new fragment particle. The original body is the last body in this file with event = -1. This body is too small and it is directly removed from the simulation.

Each new created fragment gets a new index number.
This file permits to recronstruct the collision and fragmentation history of every particle.


##The master file: master.out
The master file contains information about the used hardware and simulation progress. If an error occurs then the master file contains more details about that. 

# Irregular coordinate output #
In the 'param.dat' file can be specified the name of a calendar file, using the argument 'Irregular output calendar = <fileName>'
This file must contain line by line the desired output times of the coordinates in years.
When this option is used, then output files with names OutIrr... similar to the original output files are created. They contain the coordinate outputs of the 
specified time. When the multi simulation mode is used, then the time and time-step information is only read from the first sub-simulation and applied to all simulations synchronously.
When the argument of 'Irregular output calendar' is equal to "-", or if this line is missing in the 'param.dat', then no irregular output files are generated.
Additional to the coordinate output files, also a new energy file is created with the name EnergyIrr<name>.dat. This file contains the same columns as the Energy<name>file.
When starting a new simulation, old OutIrr<name>.dat files and EnergyIrr<name>.dat files are not deleted. If the EnergyIrr<name>.dat file already exists, then the initial energy from
a new simulation is read from this file. 
 

# Restart a simulation #
A simulation can be restarted from each coordinate output file, by using the -R time step console argument or specify a restart time in the 'param.dat' file. Before you restart a simulation you can also change things in the param.dat file, but the Output name must be the same as in the original run! To be able to restart a simulation, the corresponding coordinate output file, the corresponding line in the energy file and if used, the corresponding aeGrid file, must exist.
Note that the data in the Energy-, Collisions-, Ejections-, time- and info-files are not deleted and the new data is added at the end of these files. But the coordinate output files are OVERWRITTEN with the new data. By restarting a simulation the values of E0 and the inner Energy are read out from the original run and used again.
One can also use a coordinate output file to start a new simulation run with totally different parameters by using the Output file as a new initial condition file. the Input file Format should then be of the form << t i m r x y z vx vy vz Sx Sy Sz amin amax emin emax - - - - >>

When GENGA is restarted using the console argument "-R", it changes the entry of the Lock file named lock.dat. This file must be deleted or modified when GENGA is restarted again from the same time step. This prevents from data loss by an accidental relaunch. To ignore the lock file the IgnoreLockFile Flag in the define.h file can be set to 1.

With the restart time -1, GENGA searches automatically for the last output and restarts directly from there. To determine the last output, the last entry in the time-file is used.


# Interrupting a simulation #

GENGA can be interrupted with the SIGINT signal (Ctrl-C). In this case, GENGA completes the current time step and writes one additional output. With the restart time step -1, GENGA will continue the integration starting from this output. The SIGINT signal can also be sent to GENGA when using a queuing system (e.g. SLURM).

# aeLimits #
The aeLimits values amin, amax, emin and emax are limits for the semi major axis and eccentricity for an individual particle. The values can be set in the initial condition file. If the corresponding particle spends time in the given area in a-e space, then a counter is increased.
This values can be useful in a stability analysis of planetary systems. 

# aeGrid #
The aeGrid is a semi-major axis versus eccentricity grid, and/or a semi-major axis versus inclination grid, which counts the time steps that particles spend in certain ranges in semi major axis and eccentricity or inclination. The dimensions of the grid can be set in the 'param.dat' file with the aeGrid_amin, aeGrid_amax, aeGrid_emin, aeGrid_emax, aeGrid_imin and aeGrid_imax values, where the semi-major axis 'a' is a positive value in astronomical units, the eccentricity 'e' a value between zero and one and the inclination 'i' is an angle between 0 and pi.
The resolution of the grid can be set by the Na, Ne and Ni values.
In the multi simulation mode all sub-simulations are contributing to the same files.

# The Poincare surface of section #
By setting the parameter ** poincareFlag 1 ** in the define.h file, GENGA prints the Poincare surface of section. It prints the coordinates of x and v when a particles crosses the positive x coordinate.
Note that this works only using the second order integrator, and not for test particles or in the multi simulation mode. An example of the surface of section  of 32 planetesimals can be found [here](https://www.youtube.com/watch?v=a_4cjXVDEAw).


# Gas disc #
The gas disc is implemented according Morishima et al. (2010) [From planetesimals to terrestrial planets: N-body simulations including the effects of nebular gas and giant planets](http://arxiv.org/abs/1007.0579). It supports gas drag, Type I migration and drag enhancement for small particles.
The parameters for the gas disc can be set in:

 * define.h:
    * def_Gasnr_g: Number of cells in r direction for gas grid
    * def_Gasnz_g: Number of cells in z direction for gas grid
    * def_Gasnr_p: Number of cells in r direction for particle grid
    * def_Gasnz_p: Number of cells in z direction for particle grid
    * def_h_1: scale height at 1AU for c = 1km/s
    * def_M_Enhance: factor for enhancement
    * def_Mass_pl: factor for enhancement
    * def_fMass_min: factor for enhancement 
 * param.dat:
   * Use gas disk: By setting this to 1, the gas disc] is used. Individual effects can be selected with the following parameters.
   When set to 0, then no gas effects are applied. 
   * Use gas disk potential; By setting this to 1, the gas disk potential is used.
   * Use gas disk enhancement; By setting this to 1, the gas disk enhancement is used.
   * Use gas disk drag; By setting this to 1, the gas disk drag is used.
   * Use gas disk tidal dampening; By setting this to 1, the gas disk tidal damping is used (Type I migration).
   * Gas dTau_diss: The dissipation time for the gas disc in years
   * Gas Sigma_10: The gas surface density at 1 AU Sigma_10 in g / cm^3
   * Gas alpha: The power law exponent for the gas disc, alpha 
   * Gas Mgiant: Mass limit for gas effects, in solar masses. If m > Mgiant, the gas drag and tidal dampening is not applied to that particle.
   * Gas file name:

# Test particle mode
The test particle mode can be used to speed up simulations with lots of small particles.
Particles are considered as test particles, when the mass of the particle is smaller or equal than the value of the 'Particle minimum Mass' argument.

 * Test Particle mode = 1: The test particles don't interact with other test particles or massive bodies. They feel the gravitational potential of the massive bodies.
 * Test Particle mode = 2: The test particles don't interact with other test particles. They feel the gravitational potential of the massive bodies and vice versa.

The test particle mode can be started with the '-TP 1' or '-TP 2'  console argument or by changing the 'use Test Particles' value in the 'param.dat' file. 

# Multi simulation mode
The multi simulation mode can be used to simulate a large number of small simulations with up to 16 massive particles. For each simulation a new directory is needed, containing the initial condition file and the param.dat file. Note that not all parameters in the 'param.dat' file can be chosen individually, these are only:

 * the time step
 * the number of integration steps
 * the name
 * the central mass
 * the n1 parameter
 * the n2 parameter
 * the initial condition file
 * the default rho
 * the Nmin value
 * the inner truncation radius
 * the outer truncation radius

All the other parameters are read from the simulation number 0.
To start a multi simulation run, an additional file is needed, which contains a list of all sub simulation directory names. For example:

 * path.dat:
    * sim0000
    * sim0001
    * sim0002
    * .
    * .
    * .

The simulation can then be started with './genga -M path.dat'
The sub simulations can all have a different number of bodies. If a sub simulation contains less particles than specified in the Nmin value, then this specific simulation is stopped, and the total number of simulations gets reduced.
In the multi simulation mode, the indexes of the particles should not be greater than 100.

# Additional Forces #
in the file 'force.h' can be included additional forces. In this kernel, all coordinates have already been converted into heliocentric coordinates.
In the 'param.dat' can be specified a binary number 'Use force' for different forces. The kernel is executed for Use force > 0.
Implemented are the following forces:

 * General Relativity correction: binary number 1
 * Tidal forces: binary number 2
 * Rotational Forces: binary number 4

For example 'Use force = 5' will use GR and Rotational forces, 'Use force = 2' will only use Tidal forces.

# Yarkovsky Effect #

The Yarkovsky effect is implemented with two different schemes: 
 * The first one is according to VOKROUHLICKY, MILANI, AND CHESLEY 2000 (Yarkovsky Effect on Small Near-Earth Asteroids: Mathematical
Formulation and Examples) and VOKROUHLICKYY & FARINELLA 1999 (THE YARKOVSKY SEASONAL EFFECT ON ASTEROIDAL FRAGMENTS : A NONLINEARIZED THEORY
FOR SPHERICAL BODIES). This scheme computes the Yarkovsky effect and performs a velocity kick.
 * The second scheme is according to Vokroulicky Milani Chesley 2000 (Yarkovsky Effect on Small Near-Earth Asteroids: Mathematical
Formulation and Examples). This scheme computes the drift rate da/dt and updates the Keplerian elements.

The Yarkovsky effect can be enabled by setting 'Use Yarkovsky = 1' or 'Use Yarkovsky = 2' in the param.dat' file.

The following parameters are relevant for the Yarkovsky effect and can be set in the 'param.h' file:
 * Solar Constant: Solar Constant at 1 AU in W /m^2

The following parameters are relevant for the Yarkovsky effect and can be set in the 'define.h' file:
 * Asteroid_eps: Emissivity factor
 * Asteroid_rho: density of the body in kg/m^3
 * Asteroid_C: Specific Heat Capacity in J/kg/K
 * Asteroid_A: Bond albedo
 * Asteroid_K: Thermal conductivity in W/m/K


# Poynting Robertson drag #
The Poynting Robertson drag is implemented with two different schemes, both according to BURNS, LAMY, AND SOTER, 1979 (Radiation Forces on Small Particles in the Solar System). 
 * The first scheme computes the Yarkovsky effect and performs a velocity kick.
 * The second scheme computes the drift rates da/dt and de/dt and updates the Keplerian elements. This takes longer to compute than the first scheme. 

The Poynting-Robertson drag effect can be enabled by setting 'Use Poynting-Robertson = 1' or 'Use Poynting-Robertson = 2' in the param.dat' file.

The following parameters are relevant for the Poynting-Robertson drag and can be set in the 'param.h' file:
 * Solar Constant: Solar Constant at 1 AU in W /m^2
 * Qpr: radiation pressure coefficient

When the mass of a particle is zero, then the density `Asteroid_rho`, specified in the `define.h` file, is used to calculate its mass.

# Set Elements function #
*(Attention: Update since version 3.92, units and data structure changed)*
This option can be used to modiy the orbital parameters of a body according to a precomputed data table. To enable this option, the file name must be set with the 'Set Elements file name' parameter in the 'param.dat' file. 
It orbital parameters can either be Keplerian elements (a e i Omega, omega M) or cartesian coordinates (x y z vx, vy, vz). The two sets can not be mixed. If cartesian coordinates are used, then the positions must be given in helioscentric coordinates, and the velocities in barycentric coordinates, because these are the units, which the codes uses internally.  
The elements are interpolated with a cubic interpolation scheme from that data table.


The structure of the data file must be the following:


    numer of bodies to modify, 't', element symbol 1, elements symbol 2, ...
    time 1, body 1 element 1, body 1 element 2, ..., 
    time 1, body 2 element 1, body 2 element 2, ...,
    .
    .
    .
    time 2, body 1 element 1, body 1 element 2, ..., 
    time 2, body 2 element 1, body 2 element 2, ...,
    .
    .
    .

 * The number of bodies 'n', indicates how many bodies will be modified. They are the first 'n' massive bodies in the initial condition file
 * The index of the planets can not be set. The order of the planets must correspond to the order of the initial conditions file. 
 * time is the time of the elements in years
 * elements can be:
    * a, semi-major axis in AU
    * e, eccentricity
    * i, inclination in radians
    * O, (Omega) longitude of the ascending node in radians 
    * w, (omega) argument of periapsis in radians
    * T, epoch time in days
    * m, mass in Solar masses
    * r, radius in AU
    * x, X-position in AU (helioscentric)
    * y, Y-position in AU (helioscentric)
    * z, Z-position in AU (helioscentric)
    * vx, X-velocity in AU/day * 0.0172020989 (heliocentric)
    * vy, Y-velocity in AU/day * 0.0172020989 (heliocentric)
    * vz, Z-velocity in AU/day * 0.0172020989 (heliocentric)
    * vxb, X-velocity in AU/day * 0.0172020989 (barycentric)
    * vyb, Y-velocity in AU/day * 0.0172020989 (barycentric)
    * vzb, Z-velocity in AU/day * 0.0172020989 (barycentric)
    * -, skip that column

An example data file to modiy the mass and radius of a body looks like this:

    1 t m r
    0.0000000000000000 3.0024584e-7  2.7582675517426333e-5
    2.2000000476840000 3.00262253e-7 2.75828934594789e-5
    5.3680003681180004 3.00285888e-7 2.758346952419557e-5
    9.9299211920929995 3.00319926e-7 2.7584299066671142e-5
    .
    .
    .

An example data file to modiy the semi-major axis, eccentricity and inclination of the first four bodies looks like the following, where 
the columns are, time, a0, a1, a2, a3, e0, e1, e2, e3, i0, i1, i2, i3, for the bodies 0-3:

    4 t a e i
    0    5.49973  3.17077e-05  1.03555e-06
    0    5.70011  3.10758e-05  0.00546965
    0    9.9999   3.09719e-06  0.000956204
    0    11.25    2.33299e-06  0.00194193
    100  5.49963  3.09278e-05  1.00262e-06 
    100  5.70002  3.01496e-05  0.00527889
    100  9.99984  6.81447e-06  0.000935937
    100  11.2499  7.15947e-06  0.00190979
    200  5.49954  9.73926e-05  4.8273e-06
    200  5.69991  9.89094e-05  0.00507701
    200  9.99975  7.85054e-06  0.000913772
    200  11.2498  7.8151e-06   0.00187511
    .
    .
    . 
