GENGA documentation (work in progress)
======================================

This new documentation is not yet complete.


Setup
~~~~~

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Param.rst
   Define.rst

Output Files
~~~~~~~~~~~~

.. toctree::
   :maxdepth: 2

   Files.rst

Options
~~~~~~~

.. toctree::
   :maxdepth: 2

   Collisions.rst
   Encounters.rst
   SerialGrouping.rst


Forces
~~~~~~

.. toctree::
   :maxdepth: 2

   Yarkovsky.rst
   PRdrag.rst

Small body collision model
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 2

   SmallBodies.rst


Integrator details
~~~~~~~~~~~~~~~~~~

.. toctree::
   :maxdepth: 2

   CloseEncounter.rst

Bibliography
~~~~~~~~~~~~

.. toctree::
   :maxdepth: 2

   Bibliography.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
